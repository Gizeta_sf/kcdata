require 'json'
require 'open-uri'
require 'nokogiri'

def gen_type(type, id)
  return 4 if id == 211
  return 5 if id == 212
  return 1 if type == 4
  return 2 if type == 1
  return 3 if type == 2
  return 6 if type == 3
  return 7 if type == 5
  return 1
end

$quest_arr = []
$ship = nil
$slotitem = nil
$useitem = nil
$furniture = nil
$poi_quest = nil

open('http://127.0.0.1:4000/quest/poi.json') do |poi_str|
  $poi_quest = JSON.parse(poi_str.readlines.join(''))
  open('http://kcwikizh.github.io/kcdata/ship/all.json') do |ship_str|
    $ship = JSON.parse(ship_str.readlines.join(''))
    open('http://kcwikizh.github.io/kcdata/slotitem/all.json') do |slotitem_str|
      $slotitem = JSON.parse(slotitem_str.readlines.join(''))
      open('http://kcwikizh.github.io/kcdata/useitem/all.json') do |useitem_str|
        $useitem = JSON.parse(useitem_str.readlines.join(''))
        open('http://kcwikizh.github.io/kcdata/furniture/all.json') do |furniture_str|
          $furniture = JSON.parse(furniture_str.readlines.join(''))
          open('http://127.0.0.1:4000/quest/all.json') do |quest_str|
            quest = JSON.parse(quest_str.readlines.join(''))

            quest.each do |q|
              obj = {}
              p q["id"]
              next unless $poi_quest.find {|i| i["game_id"] == q["id"]}
              obj["game_id"] = q["id"]
              obj["wiki_id"] = q["wiki_id"].scan(/\d+/)[0].length == 1 ? q["wiki_id"].gsub(/(\d)/, '0\1') : q["wiki_id"]
              obj["category"] = q["category"] == 8 ? 2 : q["category"]
              obj["type"] = gen_type(q["type"], q["id"])
              obj["name"] = q["title"]
              obj["detail"] = q["detail"]
              obj["reward_fuel"] = q["reward"][0]
              obj["reward_ammo"] = q["reward"][1]
              obj["reward_steel"] = q["reward"][2]
              obj["reward_bauxite"] = q["reward"][3]
              obj["reward_other"] = []
              q["reward2"].each do |q_r2|
                case q_r2["category"]
                when "useitem"
                  obj["reward_other"].push({"name": $useitem.find {|i| i["id"] == q_r2["id"]}["name"], "amount": q_r2["amount"]})
                when "ship"
                  obj["reward_other"].push({"name": $ship.find {|i| i["id"] == q_r2["id"]}["name"], "category": "艦娘", "amount": q_r2["amount"]})
                when "slotitem"
                  obj["reward_other"].push({"name": $slotitem.find {|i| i["id"] == q_r2["id"]}["name"], "category": "装備", "amount": q_r2["amount"]})
                when "furniture"
                  obj["reward_other"].push({"name": $furniture.find {|i| i["id"] == q_r2["id"]}["title"], "category": "家具"})
                when "other"
                  obj["reward_other"].push({"name": "#{q_r2["item"]}#{q_r2["amount"].nil? ? "" : q_r2["amount"]}"})
                else
                  names = (q_r2["list"].map do |o|
                    case o["category"]
                    when "useitem"
                      $useitem.find {|i| i["id"] == o["id"]}["name"] + (o["amount"] > 1 ? "(x#{o["amount"]})" : "")
                    when "slotitem"
                      $slotitem.find {|i| i["id"] == o["id"]}["name"] + (o["level"].nil? ? "" : "★+#{o["level"]}") + (o["amount"] > 1 ? "(x#{o["amount"]})" : "")
                    end
                  end).join(" / ")
                  obj["reward_other"].push({"name": names, "amount": q_r2["select"]})
                end
              end unless q["reward2"].nil?

              obj["prerequisite"] = q["prerequisite"]
              if $poi_quest.find {|i| i["game_id"] == q["id"]}
                obj["requirements"] = $poi_quest.find {|i| i["game_id"] == q["id"]}["requirements"]
              else
                obj["requirements"] = {}
              end
              $quest_arr.push(obj)
            end
          end
        end
      end
    end
  end
end

File.open("../quest/poi.json", 'w') do |file|
  file.write(JSON.pretty_generate($quest_arr))
end
