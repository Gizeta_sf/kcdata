require 'json'
require 'open-uri'
require 'nokogiri'

def _(origin, new)
  return origin if new.nil? || new == ""
  return new
end

open('http://api.kcwiki.moe/start2') do |api_start2|
  open('http://kcwikizh.github.io/kcdata/equiptype/all.json') do |origin|
    data = JSON.parse(api_start2.readlines[0])
    odata = JSON.parse(origin.readlines.join(''))
    data["api_mst_slotitem_equiptype"].each do |etype|
      origin_data = odata.find {|s| s["id"] == etype["api_id"]}
      if origin_data.nil?
        File.open("../_equiptype/#{etype["api_id"]}.json", 'w') do |file|
          file.write(%Q{---
layout: json
data:
  id: #{etype["api_id"]}
  name: #{etype["api_name"]}
  show_flg: #{etype["api_show_flg"]}
  chinese_name:
---})
        end
      else
        File.open("../_equiptype/#{etype["api_id"]}.json", 'w') do |file|
          file.write(%Q{---
layout: json
data:
  id: #{etype["api_id"]}
  name: #{_(origin_data["name"], etype["api_name"])}
  show_flg: #{_(origin_data["show_flg"], etype["api_show_flg"])}
  chinese_name: #{origin_data["chinese_name"]}
---})
        end
      end
    end
  end
end
