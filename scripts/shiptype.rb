require 'json'
require 'open-uri'
require 'nokogiri'

def _(origin, new)
  return origin if new.nil? || new == ""
  return new
end

open('http://api.kcwiki.moe/start2') do |api_start2|
  open('http://kcwikizh.github.io/kcdata/shiptype/all.json') do |origin|
    data = JSON.parse(api_start2.readlines[0])
    odata = JSON.parse(origin.readlines.join(''))
    data["api_mst_stype"].each do |stype|
      origin_data = odata.find {|s| s["id"] == stype["api_id"]}
      if origin_data.nil?
        File.open("../_shiptype/#{stype["api_id"]}.json", 'w') do |file|
          file.write(%Q{---
layout: json
data:
  id: #{stype["api_id"]}
  sort_no: #{stype["api_sortno"]}
  name: #{stype["api_name"]}
  scnt: #{stype["api_scnt"]}
  kcnt: #{stype["api_kcnt"]}
  equip_type:#{stype["api_equip_type"].sort{|a,b|a[0].to_i<=>b[0].to_i}.inject(""){|s, type| s += "\n    #{type[0]}: #{type[1]}" ; s}}
  chinese_name:
  english_name:
---})
        end
      else
        File.open("../_shiptype/#{stype["api_id"]}.json", 'w') do |file|
          file.write(%Q{---
layout: json
data:
  id: #{stype["api_id"]}
  sort_no: #{_(origin_data["sortno"], stype["api_sortno"])}
  name: #{_(origin_data["name"], stype["api_name"])}
  scnt: #{_(origin_data["scnt"], stype["api_scnt"])}
  kcnt: #{_(origin_data["kcnt"], stype["api_kcnt"])}
  equip_type:#{_(origin_data["equip_type"], stype["api_equip_type"]).sort{|a,b|a[0].to_i<=>b[0].to_i}.inject(""){|s, type| s += "\n    #{type[0]}: #{type[1]}" ; s}}
  chinese_name: #{origin_data["chinese_name"]}
  english_name: #{origin_data["english_name"]}
---})
        end
      end
    end
  end
end
