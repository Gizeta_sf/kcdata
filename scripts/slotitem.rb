require 'json'
require 'open-uri'
require 'nokogiri'
require 'fiber'

def _(origin, new)
  return origin if new.nil? || new == ""
  return new
end

def gen_json(obj, space = 4)
  return "" if obj.nil?
  result = ""
  if obj.is_a?(Numeric) || obj.is_a?(String) || [true, false].include?(obj)
    return obj.to_s
  elsif obj.is_a?(Array) 
    obj.each do |o|
      tmp = gen_json(o, space + 2)
      result += "\n" + " " * space + "-" + (tmp.start_with?("\n") ? tmp : " #{tmp}")
    end
  else
    result += "\n" if space == 4
    obj.each do |key, value|
      if value.is_a?(Numeric)
        result += "\n" + " " * space + "#{key}: #{value}"
      elsif [true, false].include?(value)
        result += "\n" + " " * space + "#{key}: #{value}"
      elsif value.is_a?(Array)
        if value.all?{|i| i.is_a?(Numeric) || i.is_a?(String) || [true, false].include?(value)}
          result += "\n" + " " * space + "#{key}: #{value}"
        else
          result += "\n" + " " * space + "#{key}:"
          value.each do |v|
            result += "\n" + " " * (space + 2) + "-"
            tmp = gen_json(v, space + 4)
            result += (tmp.start_with?("\n") ? tmp : " #{tmp}")
          end
        end
      elsif value.is_a?(String)
        result += "\n" + " " * space + "#{key}: #{value}"
      else
        result += "\n" + " " * space + "#{key}:#{gen_json(value, space + 2)}"
      end
    end
  end
  return result
end

data = Fiber.new do
  open('http://api.kcwiki.moe/start2') do |u|
    d = JSON.parse(u.readlines[0])
    Fiber.yield d
  end
end.resume

odata = Fiber.new do
  open('http://kcwikizh.github.io/kcdata/slotitem/all.json') do |u|
    d = JSON.parse(u.readlines.join(''))
    Fiber.yield d
  end
end.resume

wdata = Fiber.new do
  open('https://raw.githubusercontent.com/TeamFleet/WhoCallsTheFleet-DB/master/db/items.nedb') do |u|
    o = {}
    u.readlines.each do |t|
      o2 = []
      j = JSON.parse(t)
      id = j["id"]
      if j["improvable"]
        j["improvement"].each do |impr|
          o3 = {}
          o3["consume"] = {}
          o3["consume"]["resource"] = impr["resource"][0]
          
          # 初期
          o3["consume"]["useitem"] = []
          o4 = []
          o5 = {}
          o5["id"] = 3
          o5["amount"] = [impr["resource"][1][0], impr["resource"][1][1]]
          o4.push(o5)
          o5 = {}
          o5["id"] = 4
          o5["amount"] = [impr["resource"][1][2], impr["resource"][1][3]]
          o4.push(o5)
          o3["consume"]["useitem"].push(o4)

          # 中段
          o4 = []
          o5 = {}
          o5["id"] = 3
          o5["amount"] = [impr["resource"][2][0], impr["resource"][2][1]]
          o4.push(o5)
          o5 = {}
          o5["id"] = 4
          o5["amount"] = [impr["resource"][2][2], impr["resource"][2][3]]
          o4.push(o5)
          o3["consume"]["useitem"].push(o4)

          # 初期装备
          o3["consume"]["slotitem"] = []
          o4 = {}
          tmp1 = impr["resource"][1][4]
          tmp2 = 0
          if tmp1.is_a?(Array)
            tmp1 = impr["resource"][1][4][0][0]
            tmp2 = impr["resource"][1][4][0][1]
          else
            tmp1 = impr["resource"][1][4]
            tmp2 = impr["resource"][1][5]
          end
          if tmp2 > 0
            o4["id"] = tmp1
            o4["amount"] = tmp2
          else
            o4 = nil
          end
          o3["consume"]["slotitem"].push(o4)

          # 中段装备
          o4 = {}
          tmp1 = impr["resource"][2][4]
          tmp2 = 0
          if tmp1.is_a?(Array)
            tmp1 = impr["resource"][2][4][0][0]
            tmp2 = impr["resource"][2][4][0][1]
          else
            tmp1 = impr["resource"][2][4]
            tmp2 = impr["resource"][2][5]
          end
          if tmp2 > 0
            o4["id"] = tmp1
            o4["amount"] = tmp2
          else
            o4 = nil
          end
          o3["consume"]["slotitem"].push(o4)

          # 更新
          o3["upgrade"] = {}
          if impr["upgrade"]
            o3["upgrade"]["id"] = impr["upgrade"][0]
            o3["upgrade"]["level"] = impr["upgrade"][1]

            o3["upgrade"]["consume"] = {}
            o3["upgrade"]["consume"]["useitem"] = []
            o5 = {}
            o5["id"] = 3
            o5["amount"] = [impr["resource"][3][0], impr["resource"][3][1]]
            o3["upgrade"]["consume"]["useitem"].push(o5)
            o5 = {}
            o5["id"] = 4
            o5["amount"] = [impr["resource"][3][2], impr["resource"][3][3]]
            o3["upgrade"]["consume"]["useitem"].push(o5)

            o4 = {}
            tmp1 = impr["resource"][3][4]
            tmp2 = 0
            if tmp1.is_a?(Array)
              tmp1 = impr["resource"][3][4][0][0]
              tmp2 = impr["resource"][3][4][0][1]
              unless impr["resource"][3][4][1].nil?
                o3["upgrade"]["consume"]["useitem"].push({
                  "id" => impr["resource"][3][4][1][0].gsub("consumable_", "").to_i,
                  "amount" => impr["resource"][3][4][1][1]
                })
              end
            elsif tmp1.is_a?(String)
              tmp1 = impr["resource"][3][4]
              tmp2 = impr["resource"][3][5]
              o3["upgrade"]["consume"]["useitem"].push({
                "id" => tmp1.gsub("consumable_", "").to_i,
                "amount" => tmp2
              })
              tmp2 = 0
            else
              tmp1 = impr["resource"][3][4]
              tmp2 = impr["resource"][3][5]
            end
            if tmp2 > 0
              o4["id"] = tmp1
              o4["amount"] = tmp2
            else
              o4 = nil
            end
            o3["upgrade"]["consume"]["slotitem"] = o4
          else
            o3["upgrade"] = nil
          end

          # 日期
          o3["day"] = []
          impr["req"].each do |w|
            if w[1] == false
              0.upto(6).each do |i|
                if w[0][i]
                  o3["day"][i] = true
                else
                  o3["day"][i] = false if o3["day"][i].nil?
                end
              end
            else
              0.upto(6).each do |i|
                if w[0][i]
                  o3["day"][i] = [] if !o3["day"][i]
                  w[1].each do |s|
                    o3["day"][i].push({
                      "ship_id" => s
                    })
                  end
                else
                  o3["day"][i] = false if o3["day"][i].nil?
                end
              end
            end
          end

          o2.push(o3)
        end
      end
      o[id] = o2 == [] ? nil : o2
    end
    Fiber.yield o
  end
end.resume

def get_improvement(id, wdata)
  txt = gen_json(wdata[id])
  return txt == "nil" ? "" : txt 
end

data["api_mst_slotitem"].each do |slotitem|
  origin_data = odata.find {|s| s["id"] == slotitem["api_id"]}
  if origin_data.nil?
    File.open("../_slotitem/#{slotitem["api_id"]}.json", 'w') do |file|
      file.write(%Q{---
layout: json
order: #{slotitem["api_id"]}
data:
  id: #{slotitem["api_id"]}
  sort_no: #{slotitem["api_sortno"]}
  name: #{slotitem["api_name"]}
  type: #{slotitem["api_type"]}
  rare: #{slotitem["api_rare"]}
  broken: #{slotitem["api_broken"]}
  info: #{slotitem["api_info"]}
  usebull: #{slotitem["api_usebull"]}
  cost: #{slotitem["api_cost"]}
  distance: #{slotitem["api_distance"]}
  stats:
    taik: #{slotitem["api_taik"]}
    souk: #{slotitem["api_souk"]}
    houg: #{slotitem["api_houg"]}
    raig: #{slotitem["api_raig"]}
    soku: #{slotitem["api_soku"]}
    baku: #{slotitem["api_baku"]}
    tyku: #{slotitem["api_tyku"]}
    tais: #{slotitem["api_tais"]}
    atap: #{slotitem["api_atap"]}
    houm: #{slotitem["api_houm"]}
    raim: #{slotitem["api_raim"]}
    houk: #{slotitem["api_houk"]}
    raik: #{slotitem["api_raik"]}
    bakk: #{slotitem["api_bakk"]}
    saku: #{slotitem["api_saku"]}
    sakb: #{slotitem["api_sakb"]}
    luck: #{slotitem["api_luck"]}
    leng: #{slotitem["api_leng"]}
  improvement: #{get_improvement(slotitem["api_id"], wdata)}
  chinese_name:
---})
    end
  else
    File.open("../_slotitem/#{slotitem["api_id"]}.json", 'w') do |file|
      file.write(%Q{---
layout: json
order: #{slotitem["api_id"]}
data:
  id: #{slotitem["api_id"]}
  sort_no: #{_(origin_data["sortno"], slotitem["api_sortno"])}
  name: #{_(origin_data["name"], slotitem["api_name"])}
  type: #{_(origin_data["type"], slotitem["api_type"])}
  rare: #{_(origin_data["rare"], slotitem["api_rare"])}
  broken: #{_(origin_data["broken"], slotitem["api_broken"])}
  info: #{_(origin_data["info"], slotitem["api_info"])}
  usebull: #{_(origin_data["usebull"], slotitem["api_usebull"])}
  cost: #{_(origin_data["cost"], slotitem["api_cost"])}
  distance: #{_(origin_data["distance"], slotitem["api_distance"])}
  stats:
    taik: #{_(origin_data["taik"], slotitem["api_taik"])}
    souk: #{_(origin_data["souk"], slotitem["api_souk"])}
    houg: #{_(origin_data["houg"], slotitem["api_houg"])}
    raig: #{_(origin_data["raig"], slotitem["api_raig"])}
    soku: #{_(origin_data["soku"], slotitem["api_soku"])}
    baku: #{_(origin_data["baku"], slotitem["api_baku"])}
    tyku: #{_(origin_data["tyku"], slotitem["api_tyku"])}
    tais: #{_(origin_data["tais"], slotitem["api_tais"])}
    atap: #{_(origin_data["atap"], slotitem["api_atap"])}
    houm: #{_(origin_data["houm"], slotitem["api_houm"])}
    raim: #{_(origin_data["raim"], slotitem["api_raim"])}
    houk: #{_(origin_data["houk"], slotitem["api_houk"])}
    raik: #{_(origin_data["raik"], slotitem["api_raik"])}
    bakk: #{_(origin_data["bakk"], slotitem["api_bakk"])}
    saku: #{_(origin_data["saku"], slotitem["api_saku"])}
    sakb: #{_(origin_data["sakb"], slotitem["api_sakb"])}
    luck: #{_(origin_data["luck"], slotitem["api_luck"])}
    leng: #{_(origin_data["leng"], slotitem["api_leng"])}
  improvement: #{get_improvement(slotitem["api_id"], wdata)}
  chinese_name: #{origin_data["chinese_name"]}
---})
    end
  end
end
