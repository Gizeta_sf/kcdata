require 'json'
require 'open-uri'
require 'nokogiri'

def _(origin, new)
  return origin if new.nil? || new == ""
  return new
end

dropData = Fiber.new do
  open('https://db.kcwiki.org/drop/ship_list') do |u|
    d = JSON.parse(u.readlines[0])
    Fiber.yield d
  end
end.resume

constructionData = Fiber.new do
  open('https://db.kcwiki.org/construction/ship_list') do |u|
    d = JSON.parse(u.readlines[0])
    Fiber.yield d
  end
end.resume

open('http://api.kcwiki.moe/start2') do |api_start2|
  open('http://unlockacgweb.galstars.net/KanColle/getPictureBookInfo') do |picture_book|
  #File.open("getPictureBookInfo", :encoding => 'utf-8') do |picture_book|
    open('http://kcwikizh.github.io/kcdata/ship/all.json') do |origin|
      data = JSON.parse(api_start2.readlines[0])
      odata = JSON.parse(origin.readlines.join(''))
      books = JSON.parse(picture_book.readlines[0])["ship_list"]
      data["api_mst_shipgraph"].each do |ship_graph|
        ship = data["api_mst_ship"].find {|s| s["api_id"] == ship_graph["api_id"]}
        ship_book = books.find {|s| s["api_index_no"] == ship_graph["api_sortno"]}
        origin_data = odata.find {|s| s["id"] == ship_graph["api_id"]}
        if origin_data.nil?
          File.open("../_ship/#{ship_graph["api_id"]}.json", 'w') do |file|
            file.write(%Q{---
layout: json
data:
  id: #{ship_graph["api_id"]}
  sort_no: #{ship_graph["api_sortno"]}
  name: #{ship && ship["api_name"]}
  yomi: #{ship && (ship["api_yomi"] == "-" ? "" : ship["api_yomi"])}
  stype: #{ship && ship["api_stype"]}
  ctype: #{ship_book && ship_book["api_ctype"]}
  cnum: #{ship_book && ship_book["api_cnum"]}
  backs: #{ship && ship["api_backs"]}
  after_lv: #{ship && (ship["api_afterlv"] == 0 ? "" : ship["api_afterlv"])}
  after_ship_id: #{ship && (ship["api_aftershipid"] == "0" ? "" : ship["api_aftershipid"])}
  get_mes: #{ship && ship["api_getmes"]}
  voice_f: #{ship && ship["api_voicef"]}
  filename: #{ship_graph["api_filename"]}
  file_version: #{ship_graph["api_version"]}
  book_table_id: #{ship_book && ship_book["api_table_id"]}
  book_sinfo: #{ship_book && ship_book["api_sinfo"]}
  stats:
    taik: #{ship && ship["api_taik"]}
    souk: #{ship && ship["api_souk"]}
    houg: #{ship && ship["api_houg"]}
    raig: #{ship && ship["api_raig"]}
    tyku: #{ship && ship["api_tyku"]}
    luck: #{ship && ship["api_luck"]}
    soku: #{ship && ship["api_soku"]}
    leng: #{ship && ship["api_leng"]}
    kaih: #{ship_book && ship_book["api_kaih"]}
    tais: #{ship_book && ship_book["api_tais"]}
    slot_num: #{ship && ship["api_slot_num"]}
    max_eq: #{ship && ship["api_maxeq"]}
    after_fuel: #{ship && (ship["api_afterfuel"] == 0 ? "" : ship["api_afterfuel"])}
    after_bull: #{ship && (ship["api_afterbull"] == 0 ? "" : ship["api_afterbull"])}
    fuel_max: #{ship && ship["api_fuel_max"]}
    bull_max: #{ship && ship["api_bull_max"]}
    broken: #{ship && ship["api_broken"]}
    pow_up: #{ship && ship["api_powup"]}
    build_time: #{ship && ship["api_buildtime"]}
  graph:
    boko_n: #{ship_graph["api_boko_n"]}
    boko_d: #{ship_graph["api_boko_d"]}
    kaisyu_n: #{ship_graph["api_kaisyu_n"]}
    kaisyu_d: #{ship_graph["api_kaisyu_d"]}
    kaizo_n: #{ship_graph["api_kaizo_n"]}
    kaizo_d: #{ship_graph["api_kaizo_d"]}
    map_n: #{ship_graph["api_map_n"]}
    map_d: #{ship_graph["api_map_d"]}
    ensyuf_n: #{ship_graph["api_ensyuf_n"]}
    ensyuf_d: #{ship_graph["api_ensyuf_d"]}
    ensyue_n: #{ship_graph["api_ensyue_n"]}
    battle_n: #{ship_graph["api_battle_n"]}
    battle_d: #{ship_graph["api_battle_d"]}
    wed_a: #{ship_graph["api_weda"]}
    wed_b: #{ship_graph["api_wedb"]}
  wiki_id:
  chinese_name:
  can_drop: #{dropData.include?(ship && ship["api_name"])}
  can_construct: #{constructionData.include?(ship_graph["api_id"])}
---})
          end
        else
          File.open("../_ship/#{ship_graph["api_id"]}.json", 'w') do |file|
            file.write(%Q{---
layout: json
data:
  id: #{ship_graph["api_id"]}
  sort_no: #{_(origin_data["sort_no"], ship_graph["api_sortno"])}
  name: #{_(origin_data["name"], ship && ship["api_name"])}
  yomi: #{_(origin_data["yomi"], ship && (ship["api_yomi"] == "-" ? "" : ship["api_yomi"]))}
  stype: #{_(origin_data["stype"], ship && ship["api_stype"])}
  ctype: #{_(origin_data["ctype"], ship_book && ship_book["api_ctype"])}
  cnum: #{_(origin_data["cnum"], ship_book && ship_book["api_cnum"])}
  backs: #{_(origin_data["backs"], ship && ship["api_backs"])}
  after_lv: #{_(origin_data["after_lv"], ship && (ship["api_afterlv"] == 0 ? "" : ship["api_afterlv"]))}
  after_ship_id: #{_(origin_data["after_ship_id"], ship && (ship["api_aftershipid"] == "0" ? "" : ship["api_aftershipid"]))}
  get_mes: #{_(origin_data["get_mes"], ship && ship["api_getmes"])}
  voice_f: #{_(origin_data["voice_f"], ship && ship["api_voicef"])}
  filename: #{_(origin_data["filename"], ship_graph["api_filename"])}
  file_version: #{_(origin_data["file_version"], ship_graph["api_version"])}
  book_table_id: #{_(origin_data["book_table_id"], ship_book && ship_book["api_table_id"])}
  book_sinfo: #{_(origin_data["book_sinfo"], ship_book && ship_book["api_sinfo"])}
  stats:
    taik: #{_(origin_data["stats"]["taik"], ship && ship["api_taik"])}
    souk: #{_(origin_data["stats"]["souk"], ship && ship["api_souk"])}
    houg: #{_(origin_data["stats"]["houg"], ship && ship["api_houg"])}
    raig: #{_(origin_data["stats"]["raig"], ship && ship["api_raig"])}
    tyku: #{_(origin_data["stats"]["tyku"], ship && ship["api_tyku"])}
    luck: #{_(origin_data["stats"]["luck"], ship && ship["api_luck"])}
    soku: #{_(origin_data["stats"]["soku"], ship && ship["api_soku"])}
    leng: #{_(origin_data["stats"]["leng"], ship && ship["api_leng"])}
    kaih: #{_(origin_data["stats"]["kaih"], ship_book && ship_book["api_kaih"])}
    tais: #{_(origin_data["stats"]["tais"], ship_book && ship_book["api_tais"])}
    slot_num: #{_(origin_data["stats"]["slot_num"], ship && ship["api_slot_num"])}
    max_eq: #{_(origin_data["stats"]["max_eq"], ship && ship["api_maxeq"])}
    after_fuel: #{_(origin_data["stats"]["after_fuel"], ship && (ship["api_afterfuel"] == 0 ? "" : ship["api_afterfuel"]))}
    after_bull: #{_(origin_data["stats"]["after_bull"], ship && (ship["api_afterbull"] == 0 ? "" : ship["api_afterbull"]))}
    fuel_max: #{_(origin_data["stats"]["fuel_max"], ship && ship["api_fuel_max"])}
    bull_max: #{_(origin_data["stats"]["bull_max"], ship && ship["api_bull_max"])}
    broken: #{_(origin_data["stats"]["broken"], ship && ship["api_broken"])}
    pow_up: #{_(origin_data["stats"]["pow_up"], ship && ship["api_powup"])}
    build_time: #{_(origin_data["stats"]["build_time"], ship && ship["api_buildtime"])}
  graph:
    boko_n: #{_(origin_data["graph"]["boko_n"], ship_graph["api_boko_n"])}
    boko_d: #{_(origin_data["graph"]["boko_d"], ship_graph["api_boko_d"])}
    kaisyu_n: #{_(origin_data["graph"]["kaisyu_n"], ship_graph["api_kaisyu_n"])}
    kaisyu_d: #{_(origin_data["graph"]["kaisyu_d"], ship_graph["api_kaisyu_d"])}
    kaizo_n: #{_(origin_data["graph"]["kaizo_n"], ship_graph["api_kaizo_n"])}
    kaizo_d: #{_(origin_data["graph"]["kaizo_d"], ship_graph["api_kaizo_d"])}
    map_n: #{_(origin_data["graph"]["map_n"], ship_graph["api_map_n"])}
    map_d: #{_(origin_data["graph"]["map_d"], ship_graph["api_map_d"])}
    ensyuf_n: #{_(origin_data["graph"]["ensyuf_n"], ship_graph["api_ensyuf_n"])}
    ensyuf_d: #{_(origin_data["graph"]["ensyuf_d"], ship_graph["api_ensyuf_d"])}
    ensyue_n: #{_(origin_data["graph"]["ensyue_n"], ship_graph["api_ensyue_n"])}
    battle_n: #{_(origin_data["graph"]["battle_n"], ship_graph["api_battle_n"])}
    battle_d: #{_(origin_data["graph"]["battle_d"], ship_graph["api_battle_d"])}
    wed_a: #{_(origin_data["graph"]["wed_a"], ship_graph["api_weda"])}
    wed_b: #{_(origin_data["graph"]["wed_b"], ship_graph["api_wedb"])}
  wiki_id: #{(origin_data["wiki_id"].nil? || origin_data["wiki_id"] == "") ? "" : "\"#{origin_data["wiki_id"]}\""}
  chinese_name: #{origin_data["chinese_name"]}
  can_drop: #{dropData.include?(ship && ship["api_name"])}
  can_construct: #{constructionData.include?(ship_graph["api_id"])}
---})
          end
        end
      end
    end
  end
end
