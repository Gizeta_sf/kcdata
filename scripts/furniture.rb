require 'json'
require 'open-uri'
require 'nokogiri'

def _(origin, new)
  return origin if new.nil? || new == ""
  return new
end

open('http://api.kcwiki.moe/start2') do |api_start2|
  open('http://kcwikizh.github.io/kcdata/furniture/all.json') do |origin|
    data = JSON.parse(api_start2.readlines[0])
    odata = JSON.parse(origin.readlines.join(''))
    data["api_mst_furniture"].each do |furniture|
      f_data = data["api_mst_furnituregraph"].find {|s| s["api_id"] == furniture["api_id"]}
      origin_data = odata.find {|s| s["id"] == furniture["api_id"]}
      if origin_data.nil?
        File.open("../_furniture/#{furniture["api_id"]}.json", 'w') do |file|
          file.write(%Q{---
layout: json
data:
  id: #{furniture["api_id"]}
  type: #{furniture["api_type"]}
  no: #{furniture["api_no"]}
  title: #{furniture["api_title"]}
  description: #{furniture["api_description"]}
  rarity: #{furniture["api_rarity"]}
  price: #{furniture["api_price"]}
  saleflg: #{furniture["api_saleflg"]}
  season: #{furniture["api_season"]}
  filename: #{f_data.nil? ? nil : "\"#{f_data["api_filename"]}\""}
  file_version: "#{f_data.nil? ? nil : f_data["api_version"]}"
---})
        end
      else
        File.open("../_furniture/#{furniture["api_id"]}.json", 'w') do |file|
          file.write(%Q{---
layout: json
data:
  id: #{furniture["api_id"]}
  type: #{_(origin_data["type"], furniture["api_type"])}
  no: #{_(origin_data["no"], furniture["api_no"])}
  title: #{_(origin_data["title"], furniture["api_title"])}
  description: #{_(origin_data["description"], furniture["api_description"])}
  rarity: #{_(origin_data["rarity"], furniture["api_rarity"])}
  price: #{_(origin_data["price"], furniture["api_price"])}
  saleflg: #{_(origin_data["saleflg"], furniture["api_saleflg"])}
  season: #{_(origin_data["season"], furniture["api_season"])}
  filename: #{_(origin_data["filename"], f_data.nil? ? nil : "\"#{f_data["api_filename"]}\"")}
  file_version: #{_(origin_data["file_version"], f_data.nil? ? nil : f_data["api_version"])}
---})
        end
      end
    end
  end
end
