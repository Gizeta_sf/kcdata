require 'json'
require 'open-uri'
require 'nokogiri'

def gen_json(obj, space = 4)
  return '' if obj.nil?
  result = ""
  if obj.is_a?(Numeric) || obj.is_a?(String) || [true, false].include?(obj)
    return obj
  elsif obj.is_a?(Array) 
    obj.each do |o|
      result += "\n" + " " * space + "-" + gen_json(o, space + 2)
    end
  else
    result += "\n" if space == 4
    obj.each do |key, value|
      if value.is_a?(Numeric)
        result += "\n" + " " * space + "#{key}: #{value}"
      elsif [true, false].include?(value)
        result += "\n" + " " * space + "#{key}: #{value}"
      elsif value.is_a?(Array)
        if value.all?{|i| i.is_a?(Numeric) || i.is_a?(String) || [true, false].include?(value)}
          result += "\n" + " " * space + "#{key}: #{value}"
        else
          result += "\n" + " " * space + "#{key}:"
          value.each do |v|
            result += "\n" + " " * (space + 2) + "-"
            result += gen_json(v, space + 4)
          end
        end
      elsif value.is_a?(String)
        result += "\n" + " " * space + "#{key}: #{value}"
      else
        result += "\n" + " " * space + "#{key}:#{gen_json(value, space + 2)}"
      end
    end
  end
  return result
end

def levenshtein_distance(str1, str2)
  return 0 if str1.empty?
  return 0 if str2.empty?
  return 0 if str1 == str2

  prevRow = (0..str2.length).to_a

  str1.length.times do |i|
    nextCol = i + 1
    str2.length.times do |j|
      curCol = nextCol

      nextCol = prevRow[j] + (str1[i] == str2[j] ? 0 : 1)
      tmp = curCol + 1
      nextCol = tmp if nextCol > tmp

      tmp = prevRow[j + 1] + 1
      nextCol = tmp if nextCol > tmp
      prevRow[j] = curCol
    end
    prevRow[-1] = nextCol
  end
  return prevRow[-1]
end

def _(origin, new)
  return origin if new.nil? || new == ""
  return new
end

open('https://zh.kcwiki.moe/api.php?action=parse&page=%E4%BB%BB%E5%8A%A1&prop=wikitext&format=json') do |wiki|
  #open('http://poi.0u0.moe/dump/quests.csv') do |poi|
  File.open("quests.csv", :encoding => 'utf-8') do |poi|
    open('http://127.0.0.1:4000/quest/all.json') do |origin|
    #open('http://kcwikizh.github.io/kcdata/quest/all.json') do |origin|
      wdata = {}
      wikitext = JSON.parse(wiki.readlines[0])["parse"]["wikitext"]["*"]
      wikitext.scan(/\{任务表\|(.+?)\}\}\n[^\|]/m).each do |quest|
        quest_text = quest[0].gsub("\n", '').gsub(/\[\[[^\|\]]+\|(.+?)（(.+?)）\]\]/, '\1(\2)').gsub(/\[\[[^\|\]]+\|(.+?)\]\]/, '\1')#.gsub(/\[\[[^\|\]]+\|link=(.+?)\]\]/, '\1').gsub(/\[\[([^\|\]]+?)\|.+?\]\]/, '\1')
        id = quest_text.match(/编号.*?=([^\|]*)/)[1].strip
        wdata[id] = {}
        wdata[id]["pre"] = quest_text.scan(/前置.*?=([^\|]*)/).map(&:first).map(&:strip).reject(&:empty?)
        wdata[id]["title"] = quest_text.match(/日文任务名字.*?=([^\|]*)/)[1].strip
        wdata[id]["content"] = quest_text.match(/日文任务说明.*?=([^\|]*)/)[1].strip
        wdata[id]["title_zh"] = quest_text.match(/中文任务名字.*?=([^\|]*)/)[1].strip
        wdata[id]["content_zh"] = quest_text.match(/中文任务说明.*?=([^\|]*)/)[1].strip.gsub(/\[\[(.+?)\]\]/, '\1').gsub(/'''(.+?)'''/, '\1')#.gsub(/\[\[.+?\|(.+?)\]\]/, '\1').gsub(/<(.+?)>(.+?)<\/p>/, '\1').gsub(/<br(.+?)>/, "<br>")
        wdata[id]["reward"] = [0, 0, 0, 0]
        wdata[id]["reward"][0] = quest_text.match(/燃料.*?=([^\|]*)/)[1].strip.to_i
        wdata[id]["reward"][1] = quest_text.match(/弹药.*?=([^\|]*)/)[1].strip.to_i
        wdata[id]["reward"][2] = quest_text.match(/钢铁.*?=([^\|]*)/)[1].strip.to_i
        wdata[id]["reward"][3] = quest_text.match(/铝.*?=([^\|]*)/)[1].strip.to_i
      end

      pdata = {}
      poi.readlines.drop(1).each do |text|
        arr = text.split(',')
        id = arr[0].to_i
        pdata[id] = {}
        pdata[id]["title"] = arr[1]
        pdata[id]["detail"] = arr[2..-3].join(',')
        pdata[id]["category"] = arr[-2].to_i
        pdata[id]["type"] = arr[-1].to_i
      end

      odata = JSON.parse(origin.readlines.join(''))

      pdata.each do |pid, pvalue|
        origin_data = odata.find {|s| s["id"] == pid}
        unless origin_data.nil?
          pvalue["wid"] = origin_data["wiki_id"]
          unless wdata[origin_data["wiki_id"]].nil?
            wdata[origin_data["wiki_id"]]["pid"] = pid
          end
        end
      end

      wdata.each do |key, value|
        next unless value["pid"].nil?
        min = 999999
        mink = nil
        minv = nil
        pdata.each do |pkey, pvalue|
          next unless pvalue["wid"].nil?
          val = levenshtein_distance(value['content'], pvalue['detail'])
          if min > val
            mink = pkey
            minv = pvalue
            min = val
          end
        end
        if mink
          minv["wid"] = key
          value["pid"] = mink
        end
      end

      wdata["C10"]["pid"] = 313

      def fix(w, p, wid, pid)
        p wid if w[wid].nil?
        w[wid]["pid"] = pid
        p[pid]["wid"] = wid
      end

      pdata.each do |key, value|
        origin_data = odata.find {|s| s["id"] == key}
        if origin_data.nil?
          File.open("../_quest/#{key}.json", 'w') do |file|
            w = wdata[value["wid"]]
            if value["wid"].nil?
              p key
              next
            end
            file.write(%Q{---
layout: json
data:
  id: #{key}
  category: #{value["category"]}
  type: #{value["type"]}
  title: #{value["title"]}
  detail: #{value["detail"]}
  reward: [#{w["reward"].join(', ')}]
  reward2:
  prerequisite: #{w["pre"].length == 0 ? "" : "[" + w["pre"].map{|i|wdata[i].nil? ? -1 : wdata[i]["pid"]}.sort.join(', ') + "]"}
  wiki_id: #{value["wid"]}
  chinese_title: #{w["title_zh"]}
  chinese_detail: #{w["content_zh"]}
---})
          end
        else
          File.open("../_quest/#{key}.json", 'w') do |file|
            w = wdata[value["wid"]]
            if value["wid"].nil? || w.nil?
              p key
              next
            end
            file.write(%Q{---
layout: json
data:
  id: #{origin_data["id"]}
  category: #{_(origin_data["category"], value["category"])}
  type: #{_(origin_data["type"], value["type"])}
  title: #{_(origin_data["title"], value["title"])}
  detail: #{_(origin_data["detail"], value["detail"])}
  reward: #{_(origin_data["reward"], "[" + w["reward"].join(', ') + "]")}
  reward2:#{gen_json(origin_data["reward2"])}
  prerequisite: #{_(origin_data["prerequisite"], w["pre"].length == 0 ? "" : "[" + w["pre"].map{|i|wdata[i].nil? ? -1 : wdata[i]["pid"]}.sort.join(', ') + "]")}
  wiki_id: #{_(origin_data["wiki_id"], w["id"])}
  chinese_title: #{_(origin_data["chinese_title"], w["title_zh"])}
  chinese_detail: #{_(origin_data["chinese_detail"], w["content_zh"])}
---})
          end
        end
      end
    end
  end
end
