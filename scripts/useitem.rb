require 'json'
require 'open-uri'
require 'nokogiri'

def _(origin, new)
  return origin if new.nil? || new == ""
  return new
end

open('http://api.kcwiki.moe/start2') do |api_start2|
  open('http://kcwikizh.github.io/kcdata/useitem/all.json') do |origin|
    data = JSON.parse(api_start2.readlines[0])
    odata = JSON.parse(origin.readlines.join(''))
    data["api_mst_useitem"].each do |useitem|
      origin_data = odata.find {|s| s["id"] == useitem["api_id"]}
      if origin_data.nil?
        File.open("../_useitem/#{useitem["api_id"]}.json", 'w') do |file|
          file.write(%Q{---
layout: json
data:
  id: #{useitem["api_id"]}
  usetype: #{useitem["api_usetype"]}
  category: #{useitem["api_category"]}
  name: #{useitem["api_name"]}
  description:
    - "#{useitem["api_description"][0]}"
    - "#{useitem["api_description"][1]}"
  price: #{useitem["api_price"]}
  chinese_name:
  chinese_description:
---})
        end
      else
        File.open("../_useitem/#{useitem["api_id"]}.json", 'w') do |file|
          file.write(%Q{---
layout: json
data:
  id: #{useitem["api_id"]}
  usetype: #{_(origin_data["usetype"], useitem["api_usetype"])}
  category: #{_(origin_data["category"], useitem["api_category"])}
  name: #{_(origin_data["name"], useitem["api_name"])}
  description:
    - "#{_(origin_data["description"][0], useitem["api_description"][0])}"
    - "#{_(origin_data["description"][1], useitem["api_description"][1])}"
  price: #{_(origin_data["price"], useitem["api_price"])}
  chinese_name: #{origin_data["chinese_name"]}
  chinese_description: #{origin_data["chinese_description"]}
---})
        end
      end
    end
  end
end
